Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '11.0'
s.name = "KruzrDriveDetectionFramework"
s.summary = "KruzrDriveDetectionFramework detects driving."
s.requires_arc = true

# 2
s.version = "0.1.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Akash Deep" => "akash@gmail.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://github.com/TheCodedSelf/RWPickFlavor"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source  = { :Git => 'https://gitlab.com/akashi3t/ios-kruzr-drive-framework.git', :branch=>'master'}

# 7
s.framework = "UIKit"


s.ios.vendored_frameworks = 'KruzrDriveDetectionFramework.framework'
# 10
s.swift_version = "4.2"

  s.source_files  = "Classes", "Classes/**/*.{h,m}"
  s.exclude_files = "Classes/Exclude"

end